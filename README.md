# Panazoom
Convert static images into an animated video using simple effects

### Requirements
You must have Python and the following packages installed:
   pip install pillow

You also need to have ffmpeg or avconv in your $PATH

### Usage
You can invoke panazoom as a Python script:
   python panazoom.py --help
