import re
import os
import subprocess

# https://stackoverflow.com/a/28909933/440780
def cmd_exists(cmd):
    return any(
        os.access(os.path.join(path, cmd), os.X_OK) 
        for path in os.environ["PATH"].split(os.pathsep)
    )

class VideoEncoder:
    
    def __init__(self, ffmpeg_path=None):
        if ffmpeg_path is None:
            if cmd_exists('ffmpeg'):
                self.ffmpeg = 'ffmpeg'
            elif cmd_exists('avconv'):
                self.ffmpeg = 'avconv'
            else:
                raise ValueError('Either "ffmpeg" or "avconv" need to be installed')
        else:
            self.ffmpeg = ffmpeg_path
        self.proc = None

    def start(self, outfile, framerate=1, vcodec_in='mjpeg', vcodec_out='libx264'):
        if self.proc:
            raise RuntimeError('Calling "start()" before process pipe was closed.')

        cmd = (self.ffmpeg,
               '-y',
               '-r', str(framerate),
               '-f','image2pipe',
               '-vcodec', vcodec_in,
               '-i', 'pipe:',
               '-preset', 'veryslow',
               '-vcodec', vcodec_out,
               outfile)

        self.proc = subprocess.Popen(cmd, stdin=subprocess.PIPE)

    def encode(self, framelist):
        ''' `framelist` must be an iterable of raw bytes '''
        for frame in framelist:
            self.proc.stdin.write(frame)
 
    def close(self):
        self.proc.stdin.close()
        self.proc = None
