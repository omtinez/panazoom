class ViewFinder(object):

    def __init__(self, canvas, rect=(0, 0, 1, 1)):
        self.rect = None
        self.aspect = None
        self.canvas = canvas
        self.set_rect(rect)

    def set_rect(self, rect):
        self.rect = rect
        self.aspect = (rect[2] - rect[0]) / float(rect[3] - rect[1])

    def fit_left(self, height=None, steps=1):
        if height is None:
            height = self.rect[1]
        width = int(self.aspect * height)
        states = self.translation((0, 0, width, height), steps)
        return list(states)

    def fit_right(self, height=None, steps=1):
        if height is None:
            height = self.rect[3] - self.rect[1]
        width = int(self.aspect * height)
        states = self.translation((self.canvas[0] - width, 0, self.canvas[0], height), steps)
        return list(states)

    @staticmethod
    def point_translation(pt0, pt1, steps=100):
        x_diff = pt1 - pt0
        x_step = x_diff / float(steps)

        x_curr = pt0
        for _ in range(steps):
            yield int(x_curr)
            x_curr += x_step

    def translation(self, target, steps=100):
        curr = self.rect
        states = zip(
            *(ViewFinder.point_translation(pt0, pt1, steps=steps) for pt0, pt1 in zip(curr, target))
        )
        for box in states:
            self.set_rect(box)
            yield box

        # Send final update
        yield target
        self.set_rect(target)
