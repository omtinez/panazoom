from viewfinder import ViewFinder
from videoencoder import VideoEncoder
from PIL import Image, ImageFilter
from io import BytesIO

class VideoEffects(object):
    
    def __init__(self, image, rect=None):
        rect = rect or (0, 0, image.width, image.height)
        self.view_finder = ViewFinder((image.width, image.height), rect)
        self.img = image
        self.frames = []
        
    def clear(self):
        self.frames = []

    def still(self, rect=None, frames=60):
        rect = rect or self.view_finder.rect
        for _ in range(frames):
            self.frames.append(self.img.crop(rect))
        self.view_finder.set_rect(rect)

    def translation(self, rect, frames=60):
        for box in self.view_finder.translation(rect, steps=frames):
            self.frames.append(self.img.crop(box))

    def center(self, rect, frames=60):
        width, height = rect[2] - rect[0], rect[3] - rect[1]
        margins = (self.img.width - width) / 2, (self.img.height - height) / 2
        target_rect = (
            margins[0], margins[1], max(0, self.img.width - margins[0]), max(0, self.img.height - margins[1]))
        self.translation(target_rect, frames=frames)

    def blur(self, radius, frames=60):
        img = self.img.crop(self.view_finder.rect)
        for i, _ in enumerate(range(frames)):
            self.frames.append(img.filter(ImageFilter.GaussianBlur((i + 1) * radius / frames)))

    def focus(self, radius, frames=60):
        img = self.img.crop(self.view_finder.rect)
        for i, _ in enumerate(range(frames)):
            self.frames.append(img.filter(ImageFilter.GaussianBlur(radius - (i + 1) * radius / frames)))

    def render(self, outfile, resolution=None, framerate=30, start=0, end=None):
        resolution = resolution or (self.img.width, self.img.height)
        end = end or len(self.frames)

        enc = VideoEncoder()
        enc.start(outfile, framerate=framerate)
        for i, frame in enumerate(self.frames):
            if i < start or i > end:
                continue
            with BytesIO() as output:
                frame.resize(resolution).save(output, 'JPEG')
                data = output.getvalue()
                enc.encode([data])
        enc.close()
