import sys
import random
import argparse
from PIL import Image
from videoeffects import VideoEffects

def main(args):

    parser = argparse.ArgumentParser(
        description='Convert static images into an animated video using simple effects.')
    parser.add_argument('image', type=str,
                        help='path to image file')
    parser.add_argument('output', type=str,
                        help='path to output file')
    parser.add_argument('--width', dest='width', type=int, default=720,
                        help='target width for final video resolution')
    parser.add_argument('--height', dest='height', type=int, default=1280,
                        help='target height for final video resolution')
    parser.add_argument('--effect', dest='effect', type=str, default='random',
                        help='video effect to apply to input image, valid effects are: random, pan, zoom, blur')
    parser.add_argument('--duration', dest='duration', type=float, default=2000,
                        help='duration in milliseconds of the video effect')
    parser.add_argument('--loop', dest='loop', action='store_true',
                        help='make resulting video loop back to original state')
    args = parser.parse_args(args)

    img = Image.open(args.image)
    res = args.width, args.height

    # Scale image to the equivalent of CSS cover
    scale = max(res[0] / float(img.width), res[1] / float(img.height))
    img = img.resize((int(img.width * scale), int(img.height * scale)))

    # Load image into video effects object
    vid = VideoEffects(img)
    
    # Parse effect
    known_effects = ['pan', 'zoom', 'blur']
    effect = random.choice(known_effects) if args.effect == 'random' else args.effect

    # Compute number of frames needed per effect
    num_frames = int(args.duration / 1000 * 30)

    if effect == 'pan':
        aspect_vid = res[0] / res[1]
        aspect_img = img.width / img.height
        horizontal = aspect_vid < aspect_img
        init_rect = (0, 0, res[0], res[1])
        if horizontal:
            last_rect = (img.width - res[0], 0, img.width, img.height)
        else:
            last_rect = (0, img.height - res[1], img.width, img.height)

        # Place view finder at the desired initial location but do not record that translation
        vid.translation(init_rect)
        vid.clear()

        # Record translation towards target location in view finder
        vid.translation(last_rect, frames=num_frames)

        # If loop, go back to initial location in view finder
        if args.loop:
            vid.translation(init_rect, frames=num_frames)

    # Zooming effect
    elif effect == 'zoom':

        # Start centered
        vid.center((0, 0, res[0], res[1]))
        vid.clear()

        # Zoom out to the point where we can see the full image
        factor = min(res[0] / float(img.width), res[1] / float(img.height))
        rect = (0, 0, res[0] / factor, res[1] / factor)
        vid.center(rect, frames=num_frames)

        # If loop, go back to initial location in view finder
        if args.loop:
            vid.center((0, 0, res[0], res[1]), frames=num_frames)

    # Blur effect
    elif effect == 'blur':

        frames_per_transition = int(num_frames / 2)

        # Start blurred and centered
        vid.center((0, 0, res[0], res[1]))
        vid.blur(50)
        vid.clear()

        # Make image sharp
        vid.focus(50, frames=frames_per_transition)

        # Stay on image for a second
        vid.still(frames=frames_per_transition)

        # If loop, go back to blurred image
        if args.loop:
            vid.still(frames=frames_per_transition)
            vid.blur(50, frames=frames_per_transition)

    else:
        raise ValueError('Unknown effect: "%s"' % args.effect)

    vid.render(args.output, resolution=res)

if __name__ == '__main__':
    main(sys.argv[1:])