import os
import sys
import argparse
import panazoom

def main(args):

    parser = argparse.ArgumentParser(
        description='Utility used to convert all images in a folder to videos.')
    parser.add_argument('images', type=str,
                        help='path to folder containing image files')
    parser.add_argument('output', type=str,
                        help='path to output folder for resulting videos')
    parser.add_argument('--width', dest='width', type=int, default=720,
                        help='target width for final video resolution')
    parser.add_argument('--height', dest='height', type=int, default=1280,
                        help='target height for final video resolution')
    parser.add_argument('--effect', dest='effect', type=str, default='random',
                        help='video effect to apply to input image, valid effects are: random, pan, zoom, blur')
    parser.add_argument('--duration', dest='duration', type=float, default=2000,
                        help='duration in milliseconds of the video effect')
    parser.add_argument('--loop', dest='loop', action='store_true',
                        help='make resulting video loop back to original state')
    argsp = parser.parse_args(args)

    extensions = ['jpg', 'png']
    files = [fname for fname in os.listdir(argsp.images) if fname[-3:] in extensions]
    for i, fname in enumerate(files):
        fname = os.path.join(argsp.images, fname)
        output = os.path.join(argsp.output, 'P%d.mp4' % (i + 1))
        panazoom.main([fname, output] + args[2:])

if __name__ == '__main__':
    main(sys.argv[1:])